$( document ).ready(function() {

  //Menu Responsivo
  $('.open-menu').click(function(){
    $('.main-menu').show("fade", 500);
    $('.main-menu > ul').addClass('transform');
    $('.conteudo-site').addClass('menu-aberto');
    return false;
  });
  $('.close-menu').click(function(){
    $('.main-menu').hide("fade", {direction: "right"}, 500);
    $('.main-menu > ul').removeClass('transform');
    return false;
  });

  //Fixar Menu no Header
  var lastScrollTop = 0;
  $(window).scroll(function(event) {
    var st = $(this).scrollTop();
    if (st > 100) {
      $("header").addClass('fixed');
    } else {
      $("header").removeClass('fixed');
    }
    lastScrollTop = st;
  });

  //Botão topo

  var btn = $('#top-button');

  $(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
      btn.css('opacity', '1');
    } else {
      btn.css('opacity', '0');
    }
  });

  btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop:0}, '300');
  });



  //Carousel

  // Banner Principal

  $('.main-banner.owl-carousel').owlCarousel({
    loop:false,
    margin:0,
    dots: true,
    nav:false,
    center: true,
    lazyLoad: false,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    animateOut: 'fadeOut',
    responsive:{
      0:{
        items:1
      },
      500:{
        items:1
      },
      800:{
        items:1
      }
    }
  });


  $('.listagem-planos.owl-carousel').owlCarousel({
    loop:true,
    margin:15,
    dots: false,
    nav:true,
    lazyLoad:false,
    center: false,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    animateOut: 'fadeOut',
    responsive:{
      0:{
        items:1
      },
      768:{
        items:2
      },
      992:{
        items:3
      }
    }
  });


  $('.listagem-noticias.owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    dots: true,
    nav:false,
    lazyLoad:false,
    center: false,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    animateOut: 'fadeOut',
    responsive:{
      0:{
        items:1
      },
      768:{
        items:2
      },
      992:{
        items:3
      }
    }
  });

  $('.listagem-fotos').owlCarousel({
    loop:true,
    margin:0,
    dots: false,
    nav:true,
    lazyLoad:false,
    center: false,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    animateOut: 'fadeOut',
    responsive:{
      0:{
        items:1
      },
      768:{
        items:1
      },
      992:{
        items:2
      }
    }
  });


  var widthSite = $('body').width();


  // Iniciar as animações
  new WOW().init();


  // Ajuste do container
  var larguraBody = $('body').width();
  var larguraContainer = $('.container').width();
  var larguraContainerFluid = $('.container-fluid').width();
  var paddingTexto = ((larguraContainerFluid - larguraContainer)/2);
  if (larguraBody > 767){
    $('.txt-left').css('padding-left',paddingTexto);
    $('.txt-right').css('padding-right',paddingTexto);
    $('section#banner .owl-dots').css('padding-left',paddingTexto);
  }
	
	//Máscara Telefone
	$('.tel-mask').mask('(99)9999-9999?9');
	
	
	// Ajuste na listagem de diferenciais
	$('section#diferenciais .col-md-7 ul').addClass('diferenciais');
	$('section#diferenciais .col-md-7 ul li').addClass('d-flex align-items-center mb-4 wow fadeIn');
	$('section#diferenciais .col-md-7 ul li').wrapInner('<div></div>').prepend('<div class="mr-4"><img src="http://planminas.com/novo/wp-content/uploads/2020/08/icone-check.png" alt="Check"></div>');
	
	// Ajuste na listagem de diferenciais nos planos
	$('section#diferenciais.incluso  ul').addClass('diferenciais row');
	$('section#diferenciais.incluso ul li').addClass('col-md-4 d-flex align-items-start mb-4 wow fadeIn');
	$('section#diferenciais.incluso ul li').wrapInner('<div></div>').prepend('<div class="mr-4"><img src="http://planminas.com/novo/wp-content/uploads/2020/08/icone-check.png" alt="Check"></div>');
});

