$( document ).ready(function() {
//Mapa
window.marker = null;

function initialize() {
    var map;
    var nottingham = new google.maps.LatLng(-21.353408, -43.053897);

    var style = [ 
    { "featureType": "road", 
    "elementType": 
    "labels.icon", 
    "stylers": [ 
    { "saturation": 1 }, 
    { "gamma": 1 }, 
    { "visibility": "on" }, 
    { "hue": "#e6ff00" } 
    ] 
},
{ "elementType": "geometry", "stylers": [ 
{ "saturation": -100 } 
] 
} 
];

var mapOptions = {
                // SET THE CENTER
                center: nottingham,

                // SET THE MAP STYLE & ZOOM LEVEL
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoom:16,

                // SET THE BACKGROUND COLOUR
                backgroundColor:"#eeeeee",

                // REMOVE ALL THE CONTROLS EXCEPT ZOOM
                panControl:false,
                zoomControl:true,
                mapTypeControl:false,
                scaleControl:false,
                streetViewControl:false,
                overviewMapControl:false,
                zoomControlOptions: {
                    style:google.maps.ZoomControlStyle.SMALL
                }

            }
            map = new google.maps.Map(document.getElementById('map'), mapOptions);

            // SET THE MAP TYPE
            var mapType = new google.maps.StyledMapType(style, {name:"Grayscale"});    
            map.mapTypes.set('grey', mapType);
            map.setMapTypeId('grey');

            //CREATE A CUSTOM PIN ICON
            var marker_image ='http://www.jaquebonjour.com.br/novo/wp-content/themes/jaque/img/pin.png';
            var pinIcon = new google.maps.MarkerImage(marker_image,null,null, null,new google.maps.Size(44, 66));    

            marker = new google.maps.Marker({
                position: nottingham,
                map: map,
                icon: pinIcon,
                title: 'GE+ Embalagens',
                animation: google.maps.Animation.DROP,
                position: {lat: -21.353408, lng: -43.053897}
            });
            marker.addListener('click', toggleBounce);
        }
        
        function toggleBounce() {
            if (marker.getAnimation() !== null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }

        google.maps.event.addDomListener(window, 'load', initialize);
        

    });