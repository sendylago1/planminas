<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || false === wc_get_loop_product_visibility( $product->get_id() ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php $destaque = ''; if( get_field('destacar') != '' ) { $destaque = 'destaque'; }?>
	<?php wc_product_class( $destaque, $product ); ?>>

	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_id() ), 'full' ); ?>
	<div class="foto" style="background-image: url(<?php echo $image[0]; ?>)"></div>
	<div class="info bg-purple">
		<div class="titulo font-size-25 text-center font-weight-700 color-white text-uppercase mb-3">
			<?php echo substr( $product->get_title(), 0, strpos($product->get_title(), ' ') ); ?>
			<b class="font-size-30 d-block font-weight-700 mt-3">
				<?php echo substr( $product->get_title(), strpos($product->get_title(), ' ') ); ?>		
			</b>
		</div>
		<small class="d-block color-branca text-center font-size-12"><?php the_field('adesao'); ?></small>
		<div class="preco font-size-20 color-white text-uppercase my-5 text-center">
			R$ <b class="font-size-50 font-weight-600"><?php the_field('mensal'); ?></b> / mês
		</div>
		<div class="text-center">
			<a href="<?php the_permalink(); ?>" class="cta mb-3" title="Adquirir">Adquirir</a> <br>
			<a href="<?php the_permalink(); ?>" class="cta bg-transparent" title="Saiba mais">Saiba mais</a>
		</div>
	</div>
</li>
