<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

?>
<!-- Breadcrumbs -->
<section id="breadcrumbs" class="bg-lgrey text-uppercase font-size-12 font-weight-500">
	<div class="container">
		<a href="<?php echo network_site_url(); ?>" class="d-inline mr-3" title="Página Inicial">Home</a> <span class="mr-3 font-weight-700">+</span> <span>Planos</span>
	</div>
</section>
<section id="planos" class="internas">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h2 class="section-title mb-5 wow fadeInDown">Faça<b>seu plano</b></h2>
			</div>
			<div class="col-md-8 d-md-flex align-items-center wow fadeInDown">
				<div class="mr-4 mb-3">
					<img src="<?php echo get_template_directory_uri(); ?>/img/icone-call.png" alt="Central de Atendimento" style="max-width: fit-content;">
				</div>
				<div>
					Todos os nossos planos são completos. Entre em contato com nossa central de atendimento e <b class="text-uppercase">contrate agora o seu plano</b>.
				</div>
			</div>
		</div>

		<?php
		if ( woocommerce_product_loop() ) {

			woocommerce_product_loop_start();

				while ( have_posts() ) {
					the_post();

					/**
					 * Hook: woocommerce_shop_loop.
					 */
					do_action( 'woocommerce_shop_loop' );

					wc_get_template_part( 'content', 'product' );
				}

			woocommerce_product_loop_end();

			/**
			 * Hook: woocommerce_after_shop_loop.
			 *
			 * @hooked woocommerce_pagination - 10
			 */
			do_action( 'woocommerce_after_shop_loop' );
		}  else {
			/**
			 * Hook: woocommerce_no_products_found.
			 *
			 * @hooked wc_no_products_found - 10
			 */
			do_action( 'woocommerce_no_products_found' );
		} ?>
	</div>
</section>

<?php get_footer();
