<?php get_header();
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
?>

<style type="text/css">
header .main-menu > ul > li:nth-of-type(6) > a:before{
	opacity: 1;
}

header .main-menu > ul > li:nth-of-type(6) > a:after{
	width: 100%;
}
</style>


<!-- Breadcrumbs -->
<section id="breadcrumbs" class="bg-lgrey text-uppercase font-size-12 font-weight-500">
  <div class="container">
    <a href="<?php bloginfo('url'); ?>" class="d-inline mr-3" title="Página Inicial">Home</a>
    <span class="mr-3 font-weight-700">+</span> <a href="<?php bloginfo('url'); ?>/noticias" class="d-inline mr-3" title="Notícias">Notícias</a>
    <span class="mr-3 font-weight-700">+</span> <span><?php the_title(); ?></span>
  </div>
</section>

<!-- Notícias -->
<section id="detalhes-noticias">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="text-center">
          <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" class="img-fluid mb-4">
        </div>
        <div class="color-red font-size-15 my-4">
          <?php the_time('j/m/Y'); ?>
        </div>
        <h1 class="font-size-30 font-weight-600 mb-4"><?php the_title(); ?></h1>
        <?php the_content(); ?>
      </div>
      <div class="col-md-4">
        <div class="sidebar p-5">
          <div class="search mr-lg-3">
            <i class="fas fa-search font-size-12"></i> 	<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" class="d-inline">
    						<input type="text" class="form-control" placeholder="Buscar Notícia" required name="s" />
    						<input type="hidden" name="post_type" value="post" />
    					</form>
          </div>
          <h2 class="font-size-25 font-weight-600 mb-3 mt-5">Categorias</h2>
          <ul class="disc">
          <?php wp_list_categories(array('title_li' => '') ); ?>
          </ul>
          <h2 class="font-size-25 font-weight-600 mb-3 mt-5">Tags</h2>
          <?php the_tags( '', ' ', '' ); ?>
          <div class="text-center mt-5">
            <a href="<?php bloginfo('url'); ?>/planos" class="cta" title="Nossos planos">Nossos planos</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Notícias -->
<section id="noticias" class="paginas-internas bg-grey">
  <div class="container">
    <h2 class="section-title mb-5 wow fadeInDown">Leia mais<b>notícias</b></h2>
    <ul class="listagem-noticias row">
      <?php
      query_posts(array(
        'post_type' => 'post',
        'showposts' => 3,
        'post__not_in' => array( $post->ID )
      ));
      if (have_posts()) : while (have_posts()) : the_post();
      $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
      ?>
      <li class="col-lg-4 col-md-6 mb-5">
        <a href="<?php the_permalink();?>" title="Saiba mais">
				<div class="foto" style="background-image: url(<?php echo $image[0]; ?>)"> </div>
				<div class="info bg-white">
					<div class="data color-red font-size-12">
					 <?php the_time('j/m/Y'); ?>
					</div>
					<h3 class="font-size-20 font-weight-600"><?php the_title(); ?> </h3>
				   <div class="resumo">
					 <?php the_excerpt(); ?>
				   </div>
					<span class="botao-final"><i class="bg-red">+</i>  Saiba mais</span>
				</div>
				</a>
    </li>
  <?php endwhile; endif; wp_reset_query(); ?>
</ul>
</div>
</section>


<?php get_footer(); ?>
