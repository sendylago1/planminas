<?php
/*
Template Name: Home
*/

get_header(); ?>


<!-- Banner -->
<section id="banner">
  <ul class="main-banner owl-carousel">
    <?php
    query_posts('post_type=banners_home&showposts=-1');
    if (have_posts()) : while (have_posts()) : the_post();
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
    ?>
    <li>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 txt-left  wow fadeIn d-md-flex align-items-center">
            <div>
              <h2 class="font-size-60 mb-0 font-weight-500 color-purple text-uppercase"><?php the_title(); ?> <i class="far fa-heart color-red detalhe"></i></h2>
              <h3 class="font-size-70 font-weight-700 color-purple text-uppercase"><?php the_field('subtitulo'); ?></h3>
                <div class="font-size-20 font-weight-600 font-gabriela color-purple my-md-5">
                  <?php the_excerpt(); ?>
                </div>
                <a href="<?php the_field('link'); ?>" class="cta" title="<?php the_field('texto_botao'); ?>"><?php the_field('texto_botao'); ?></a>
              </div>
            </div>
            <div class="col-md-6 imagem">
              <img src="<?php echo $image[0]; ?>" class="img-fluid" alt="<?php the_title(); ?>">
            </div>
          </div>
        </div>
      </li>
    <?php endwhile; endif; wp_reset_query(); ?>
  </ul>
</section>

<!-- Planos -->
<section id="planos">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <h2 class="section-title mb-5 wow fadeInDown">Faça<b>seu plano</b></h2>
      </div>
      <div class="col-md-8 d-md-flex align-items-center wow fadeInDown">
        <div class="mr-4 mb-3">
          <img src="<?php echo get_template_directory_uri(); ?>/img/icone-call.png" alt="">
        </div>
        <?php
        query_posts('pagename=planos');
        if (have_posts()) : while (have_posts()) : the_post();
        ?>
        <div>
          <?php the_content(); ?>
        </div>
      <?php endwhile; endif; wp_reset_query(); ?>
    </div>
  </div>
  <ul class="listagem-planos owl-carousel">
    <?php
    query_posts('post_type=nossos_planos&showposts=-1');
    if (have_posts()) : while (have_posts()) : the_post();
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
    ?>
    <li 
		<?php
		if( get_field('destacar') != '' ) {
			echo 'class="destaque"';
		}
		?>
		>
      <div class="foto" style="background-image: url(<?php echo $image[0]; ?>)"></div>
      <div class="info bg-purple">
        <!--<div class="numero font-size-30 color-white font-weight-700">0.2</div>-->
        <div class="titulo font-size-25 text-center font-weight-700 color-white text-uppercase mb-3">
          Plan <b class="font-size-30 d-block font-weight-700 mt-3"><?php the_title(); ?></b>
        </div>
        <small class="d-block color-branca text-center font-size-12"><?php the_field('adesao'); ?></small>
        <div class="preco font-size-20 color-white text-uppercase my-5 text-center">
          R$ <b class="font-size-50 font-weight-600"><?php the_field('mensal'); ?></b> / mês
        </div>
        <div class="text-center">
          <a href="<?php the_permalink(); ?>" class="cta mb-3" title="Adquirir">Adquirir</a> <br>
          <a href="<?php the_permalink(); ?>" class="cta bg-transparent" title="Saiba mais">Saiba mais</a>
        </div>
      </div>
    </li>
  <?php endwhile; endif; wp_reset_query(); ?>
</ul>
</div>
</section>

<!-- Benefícios -->
<section id="beneficios">
  <div class="container">
    <div class="row align-items-center">
      <?php
      query_posts('pagename=plan-minas');
      if (have_posts()) : while (have_posts()) : the_post();
      ?>
      <?php for($i=1;$i<=3;$i++){?>
        <?php if (get_field('titulod_'.$i) != ''){ ?>
          <div class="col-lg-4 mb-4 color-purple text-center text-uppercase wow fadeIn">
            <div class="icone mb-4">
              <div class="p-3 <?php if( $i == 1){ ?>bg-purple <?php } else { ?> bg-red<?php } ?>">
                <img src="<?php the_field('iconed_'.$i);?>" alt="<?php the_field('titulod_'.$i);?>" class="img-fluid">
              </div>
            </div>
            <h3 class="text-center font-size-25 font-weight-700"><?php the_field('titulod_'.$i);?></h3>
            <p><?php the_field('subtitulod_'.$i);?></p>
          </div>
        <?php }} ?>
      <?php endwhile; endif; wp_reset_query(); ?>
    </div>
  </div>
</section>

<!-- Central de Atendimento -->
<section id="central-atendimento">
  <div class="container">
    <div class="row color-white ">
      <div class="col-xl-5 text-uppercase mb-3 wow fadeInDown">
        <h3 class="font-size-25">Quer deixar sua <b class="color-red">família</b> integralmente protegida?</h3>
        <p>
          Fale conosco agora mesmo!
        </p>
      </div>
      <div class="col-xl-7">
        <?php
        query_posts('pagename=contato');
        if (have_posts()) : while (have_posts()) : the_post();
        ?>
        <div class="row">
          <div class="col-md-6 mb-4 d-md-flex align-items-center  wow fadeInDown">
            <div class="bg-red icone mr-3">
              <i class="fas fa-phone-alt"></i>
            </div>
            <div class="font-size-14 letter-spacing-1">
              Central de Atendimento <br>
              <div class="font-size-25">
                <?php the_field('telefone_central'); ?>
              </div>
            </div>
          </div>
          <div class="col-md-6 mb-4 d-md-flex align-items-center  wow fadeInDown">
            <div class="bg-red icone mr-3">
              <img src="<?php echo get_template_directory_uri(); ?>/img/24h.png" alt="">
            </div>
            <div class="font-size-14 letter-spacing-1">
              Funerária <b class="color-red">24 horas</b> <br>
              <div class="font-size-25">
                <?php the_field('telefone_funeraria'); ?>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile; endif; wp_reset_query(); ?>
    </div>
  </div>
</div>
</section>

<!-- Sobre -->
<section id="sobre">
  <?php
  query_posts('pagename=plan-minas');
  if (have_posts()) : while (have_posts()) : the_post();
  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
  ?>
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6">
        <img src="<?php echo $image[0]; ?>" class="img-fluid mb-4 wow fadeInLeft" alt="<?php the_title(); ?>">
      </div>
      <div class="col-lg-6 txt-right">
        <h2 class="section-title mb-5 wow fadeInDown">Sobre<b>a Plan Minas</b></h2>
        <div class="wow fadeIn">
          <?php the_content(); ?>
        </div>
        <a href="<?php the_permalink(); ?>" class="cta mt-4 wow fadeInUp" title="Saiba mais">Saiba mais</a>
      </div>
    </div>
  </div>
<?php endwhile; endif; wp_reset_query(); ?>
</section>

<!-- Rede de Parceiros -->
<section id="rede-parceiros">
  <?php
  query_posts('pagename=rede-de-parceiros');
  if (have_posts()) : while (have_posts()) : the_post();
  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
  ?>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="section-title mb-5 wow fadeInDown">Rede de<b>Parceiros</b></h2>
        <div class="wow fadeIn">
          <?php the_content(); ?>
        </div>
        <a href="<?php the_permalink(); ?>" class="cta mt-4 wow fadeInUp" title="Saiba mais">Saiba mais</a>
      </div>
      <div class="col-md-6">
        <img src="<?php echo $image[0]; ?>" class="img-fluid wow fadeInRight" alt="<?php the_title(); ?>">
      </div>
    </div>
  </div>
<?php endwhile; endif; wp_reset_query(); ?>
</section>


<!-- Notícias -->
<section id="noticias">
  <div class="container">
    <h2 class="section-title mb-5 wow fadeInDown">Últimas<b>notícias do blog</b></h2>
    <ul class="listagem-noticias owl-carousel">
      <?php
      query_posts('post_type=post&showposts=6');
      if (have_posts()) : while (have_posts()) : the_post();
      $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
      ?>
      <li>
        <a href="<?php the_permalink();?>" title="Saiba mais">
				<div class="foto" style="background-image: url(<?php echo $image[0]; ?>)"> </div>
				<div class="info bg-white">
					<div class="data color-red font-size-12">
					 <?php the_time('j/m/Y'); ?>
					</div>
					<h3 class="font-size-20 font-weight-600"><?php the_title(); ?> </h3>
				   <div class="resumo">
					 <?php the_excerpt(); ?>
				   </div>
					<span class="botao-final"><i class="bg-red">+</i>  Saiba mais</span>
				</div>
				</a>
    </li>
  <?php endwhile; endif; wp_reset_query(); ?>
</ul>
</div>
</section>



<?php get_footer(); ?>
