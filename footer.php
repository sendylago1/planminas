<!-- Rodapé -->
<footer class="color-white">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<h3 class="font-size-18 font-weight-500 mb-4">Acesso rápido</h3>
				<ul>
					<?php

					wp_nav_menu(array(
						'container' => '',
						'menu'=>'Principal',
						'items_wrap' => '<ul>%3$s</ul>'
					));

					?>
				</ul>
			</div>
			<div class="col-lg-4 text-center">
				<i class="fas fa-map-marker-alt font-size-30 color-red mb-3"></i>
				<h3 class="font-size-18 font-weight-500 mb-3">Matriz</h3>
				<div class="font-size-14">
					<?php
					query_posts('pagename=contato');
					if (have_posts()) : while (have_posts()) : the_post();
					?>
					<p>
						<?php the_field('endereco'); ?>
					</p>
				<?php endwhile; endif; wp_reset_query();?>
			</div>

			<div class="redes-sociais mt-5">
				<?php
				query_posts('post_type=redes_sociais');
				if (have_posts()) : while (have_posts()) : the_post();
				?>
				<a href="<?php the_field('link'); ?>" target="_blank" alt="<?php the_title(); ?>">
					<i class="<?php the_field('icone'); ?>"></i>
				</a>
			<?php endwhile; endif; wp_reset_query();?>
		</div>

			</div>
	<div class="col-lg-4">
		<?php
		query_posts('pagename=contato');
		if (have_posts()) : while (have_posts()) : the_post();
		?>
		<h3 class="font-size-18 font-weight-500 mb-4">Fale Conosco agora mesmo!</h3>

		<div class="d-md-flex mb-4">
			<div class="bg-red icone mr-3">
				<i class="fas fa-phone-alt"></i>
			</div>
			<div class="font-size-14 letter-spacing-1">
				Central de Atendimento <br>
				<div class="font-size-25">
					<?php the_field('telefone_central'); ?>
				</div>
			</div>
		</div>
		<div class="d-md-flex mb-4">
			<div class="bg-red icone mr-3">
				<img src="<?php echo get_template_directory_uri(); ?>/img/24h.png" alt="">
			</div>
			<div class="font-size-14 letter-spacing-1">
				Funerária <b class="color-red">24 horas</b> <br>
				<div class="font-size-25">
					<?php the_field('telefone_funeraria'); ?>
				</div>
			</div>
		</div>
	<?php endwhile; endif; wp_reset_query();?>
</div>
</div>
</div>
<?php wp_footer(); ?>
</footer>

<!-- JS -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.maskedinput-1.4.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
<!-- Personalizado -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>
</body>

</html>
