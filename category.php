<?php get_header(); ?>

teste

	<!-- Trilha -->
<section id="trilha">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 offset-lg-2">
       <div class="titulo wow fadeInDown">
       <?php the_title(); ?>
      </div>
    </div>
  </div>
</div>
</section>

<!-- Produtos -->
<section id="produtos">
  <div class="container">
    <div class="row">
      <div class="col-md-4 text-center wow fadeIn">
        <img src="img/blocos-concreto.png" class="img-fluid" alt="">
        <h3>Blocos de Concreto</h3>
        <p>
          Com a função de vedação aparente, próprio para uso interno e externo de casas, edifícios residenciais e muros
        </p>
        <a href="detalhes-produtos.html" class="call-to" title="">
          Saiba mais <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
        </a>
      </div>
      <div class="col-md-4 text-center wow fadeIn">
        <img src="img/blocos-concreto.png" class="img-fluid" alt="">
        <h3>Capas de Muro</h3>
        <p>
          É um dos materiais utilizados para o acabamento e proteção externo para muros e paredes.
        </p>
        <a href="detalhes-produtos.html" class="call-to" title="">
          Saiba mais <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
        </a>
      </div>
      <div class="col-md-4 text-center wow fadeIn">
        <img src="img/blocos-concreto.png" class="img-fluid" alt="">
        <h3>Pisos Intertravados</h3>
        <p>
          É ideal para obras em áreas externas pisos industriais ou espaços com grande movimentação.
        </p>
        <a href="detalhes-produtos.html" class="call-to" title="">
          Saiba mais <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
        </a>
      </div>
      <div class="col-md-4 text-center wow fadeIn">
        <img src="img/blocos-concreto.png" class="img-fluid" alt="">
        <h3>Pisos Intertravados</h3>
        <p>
          É ideal para obras em áreas externas pisos industriais ou espaços com grande movimentação.
        </p>
        <a href="detalhes-produtos.html" class="call-to" title="">
          Saiba mais <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
        </a>
      </div>
    </div>
  </div>
</section>


<?php get_footer(); ?>
