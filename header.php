<html lang="pt_BR">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php bloginfo( 'name' ); ?></title>
	<!-- CSS -->
	<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/all.min.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/owl.theme.default.min.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/jquery.fancybox.min.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/animate.css" rel="stylesheet">
	<!-- Personalizado -->
	<link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet">

	<!-- IE8 support for HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

	<?php wp_head(); ?>

	<!-- Favicon-->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.png">
</head>

<body <?php body_class() ?>>

	<!-- Botão Topo -->
	<a href="#" id="top-button"><i class="fas fa-chevron-up"></i></a>

	<!-- Cabeçalho -->
	<header>
		<div class="topbar color-white">
			<div class="container">
				<div class="row">
					<div class="col-md-12 phones-search text-uppercase">
						<div class="search mr-lg-3">
							<i class="fas fa-search"></i>
							<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" class="mb-0"> <input type="text" class="form-control mb-0" placeholder="Buscar" required name="s" /> </form>
						</div>
						<div class="call  mr-4">
							<a href="" class="cta" title="Ligamos para você" data-toggle="modal" data-target="#modalLigacao">Ligamos para você</a>
						</div>
						<?php
						query_posts('pagename=contato');
						if (have_posts()) : while (have_posts()) : the_post();
						?>
						<div class="phone mr-4">
							<i class="fas fa-phone-alt"></i>
							<div>
								<small>Fale Conosco</small>
								<span><?php the_field('telefone_central'); ?></span>
							</div>
						</div>
						<div class="phone">
							<div><img src="<?php echo get_template_directory_uri(); ?>/img/24h.png" alt=""> </div>
							<div>
								<small>Atendimento 24h para óbitos</small>
								<span><?php the_field('telefone_funeraria'); ?></span>
							</div>
						</div>
					<?php endwhile; endif; wp_reset_query();?>
				</div>
			</div>
		</div>
	</div>
		
		
	<div class="maintop">
		<div class="container">
			<div class="row top">
				<div class="logo">
					<a href="<?php echo network_site_url(); ?>" title="<?php bloginfo( 'name' ); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" class="img-fluid" alt="<?php bloginfo( 'name' ); ?>">
					</a>
				</div>

				<div class="menu-space">
					<div class="main-menu">
						<a href="" class="close-menu" title="Fechar Menu"><i class="fas fa-times"></i></a>
						<?php

						wp_nav_menu(array(
							'container' => '',
							'menu'=>'Principal',
							'items_wrap' => '<ul>%3$s</ul>'
						));

						?>
					</div>
					<a href="" class="open-menu" title="Abrir Menu"><i class="fa fa-bars"></i></a>
				</div>
			</div>
		</div>
	</div>
</header>
	
	<!-- Modal -->
		<div class="modal fade" id="modalLigacao" tabindex="-1" role="dialog"  aria-hidden="true">
		  <div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title font-size-25 text-uppercase color-purple font-weight-800 d-block text-center">Ligamos para você</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
				<?php echo do_shortcode( '[contact-form-7 id="199" title="Ligamos para você"]' ); ?>
			  </div>
			</div>
		  </div>
		</div>
