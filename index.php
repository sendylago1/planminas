<?php get_header(); ?>


<!-- Breadcrumbs -->
<section id="breadcrumbs" class="bg-lgrey text-uppercase font-size-12 font-weight-500">
	<div class="container">
		<a href="<?php bloginfo('url'); ?>" class="d-inline mr-3" title="Página Inicial">Home</a> <span class="mr-3 font-weight-700">+</span> <span>Notícias</span>
	</div>
</section>

<!-- Notícias -->
<section id="noticias" class="paginas-internas bg-grey">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-8">
				<h2 class="section-title mb-5 wow fadeInDown">Últimas<b>notícias do blog</b></h2>
			</div>
			<div class="col-md-4">
				<div class="search mr-lg-3">
					<i class="fas fa-search font-size-12"></i>
					<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" class="d-inline">
						<input type="text" class="form-control" placeholder="Buscar Notícia" required name="s" />
						<input type="hidden" name="post_type" value="post" />
					</form>
				</div>
			</div>
		</div>
		<?php
		if (have_posts()) :
			?>
		<ul class="listagem-noticias row">
			<a href="<?php the_permalink(); ?>" title="Saiba mais">
			<?php
			while (have_posts()) : the_post();
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
				?>
			<li class="col-lg-4 col-md-6 mb-5">
				<a href="<?php the_permalink();?>" title="Saiba mais">
				<div class="foto" style="background-image: url(<?php echo $image[0]; ?>)"> </div>
				<div class="info bg-white">
					<div class="data color-red font-size-12">
					 <?php the_time('j/m/Y'); ?>
					</div>
					<h3 class="font-size-20 font-weight-600"><?php the_title(); ?> </h3>
				   <div class="resumo">
					 <?php the_excerpt(); ?>
				   </div>
					<span class="botao-final"><i class="bg-red">+</i>  Saiba mais</span>
				</div>
				</a>
			</li>
		<?php endwhile; ?>
	</ul>
	<?php the_posts_pagination(array('prev_text' => __( '<', 'textdomain' ),'next_text' => __( '>', 'textdomain' )) ); ?>
<?php else: ?>
	<h3 class="font-size-20 cor-roxo font-weight-800 text-center">Nenhum resultado encontrado</h3>
<?php endif; ?>
</div>
</section>


<?php get_footer(); ?>
