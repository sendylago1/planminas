<?php get_header(); ?>

<style type="text/css">
header .menu-principal > ul > li:nth-of-type(3) > a{
  font-weight: bold;
}
</style>

<?php
$categoria = get_queried_object();
$titulo_cat_atual = $categoria->name
?>


<!-- Trilha -->
<section id="breadcrumbs">
  <div class="container">
    <a href="<?php bloginfo('url'); ?>" title="Home">- Home</a> <a href="<?php bloginfo('url'); ?>/produtos" title="Produtos">- Produtos</a> <span>- <?php single_cat_title(); ?></span>
  </div>
</section>

<section class="bg-gradiente">
  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-2 text-center">
        <h2 class="font-size-36 cor-azul-escuro wow fadeInDown">Produtos Próleite</h2>
        <div class="wow fadeInUp">
          <p>
            Conheça aqui os produtos especialmente feitos para você e para seu negócio com a tecnologia e alta qualidade da Pró Leite.
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="produtos" class="interna">
  <div class="container">

    <div class="text-center d-block mb-5 d-md-none">
      <small> <i>Arraste para o lado para ver as outras categorias</i> </small>
    </div>

    <?php

    $taxonomy = 'categoria';
    $terms = get_terms($taxonomy,array(
      'orderby'       => 'name',
      'order'         => 'ASC',
      'hide_empty'    => true
    ));
    if ( count($terms) > 0 ){
      echo "<ul class='categorias mb-5'>";
      foreach ( $terms as $term ) {
        $titulo_cat = $term->name;
        $icone = get_field('icone', $term);
        echo '<li><a href="'.get_term_link($term->slug, $taxonomy).'" class="d-block text-center';
        if($titulo_cat_atual == $titulo_cat){
          echo ' active';
        }
        echo '">';
        echo '<i class="';
        echo $icone;
        echo ' font-size-36 mb-2"></i>'.$term->name.'</a></li>';
      }
      echo "</ul>";
    }
    ?>
    <?php
    if (have_posts()) :
      ?>
      <ul class="row listagem-produtos wow fadeIn">
        <?php while (have_posts()) : the_post();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
        ?>
        <li class="col-md-4">
          <a href="#" data-toggle="modal" data-target="#Produto<?php echo $id; ?>">
            <div class="foto">
              <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">
            </div>
            <h3 class="mt-5"><?php the_title(); ?></h3>
            <h4><?php the_field('valor'); ?></h4>
            <span><u>+</u> Confira</span>
          </a>
          <!-- Modal -->
          <div class="modal fade" id="Produto<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-body p-5">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                  </button>

                  <div class="row">
                    <div class="col-md-5 text-center">
                      <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" class="img-fluid">
                    </div>
                    <div class="col-md-7">
                      <h3 class="font-size-22 mt-5"><?php the_title(); ?></h3>
                      <h4 class="font-size-48 cor-verde mb-4 "><?php the_field('valor'); ?></h4>
                      <p>
                        <?php the_content(); ?>
                      </p>
                      <a href="https://api.whatsapp.com/send?phone=5533988341571" class="cta mt-3" target="_blank" title="Fale com um representante"><i class="fab fa-whatsapp"></i> Fale com um representante </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      <?php endwhile; ?>
    </ul>
    <?php the_posts_pagination(array('prev_text' => __( '<', 'textdomain' ),'next_text' => __( '>', 'textdomain' )) ); ?>
  <?php else: ?>
    <div class="col-md-12 wow fadeIn">
      <h3 class="titulo-18 text-center cor-grafite my-5">Nenhum resultado encontrado</h3>
    </div>
  <?php endif; ?>
</div>
</section>


<?php get_footer(); ?>
