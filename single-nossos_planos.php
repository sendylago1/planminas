<?php get_header();
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
?>

<style type="text/css">
header .main-menu > ul > li:nth-of-type(2) > a:before{
	opacity: 1;
}

header .main-menu > ul > li:nth-of-type(2) > a:after{
	width: 100%;
}
</style>


<!-- Breadcrumbs -->
<section id="breadcrumbs" class="bg-lgrey text-uppercase font-size-12 font-weight-500">
  <div class="container">
    <a href="<?php bloginfo('url'); ?>" class="d-inline mr-3" title="Página Inicial">Home</a>
    <span class="mr-3 font-weight-700">+</span> <a href="<?php bloginfo('url'); ?>/planos" class="d-inline mr-3" title="Planos">Planos</a>
    <span class="mr-3 font-weight-700">+</span> <span><?php the_title(); ?></span>
  </div>
</section>

<section id="detalhes-planos">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 pl-md-0">
          <img src="<?php echo $image[0]; ?>" class="img-fluid" alt="<?php the_title(); ?>">
          <div class="txt-left my-5">
            <?php the_field('condicoes'); ?>

            <h4 class="font-size-20 color-purple font-weight-700 my-4 text-uppercase">Verificar Disponibilidade na região através do whatsapp</h4>

            <a href="#" class="link-whatsapp font-weight-700 font-size-20"><i class="fab fa-whatsapp"></i> (24) 99999-5959</a>
          </div>
        </div>
        <div class="col-md-6 txt-right">
          <h2 class="color-purple font-size-20 text-uppercase font-weight-700 mb-3">Plano <?php the_title(); ?></h2>
          <h3 class="color-purple font-size-25 text-uppercase font-weight-700 mb-5">A partir de: <span class="color-red">R$<?php the_field('mensal'); ?></span> / mês e uma taxa de inscrição de R$<?php the_field('inscricao'); ?></h3>
          <?php the_content(); ?>
          <form>
            <h3 class="font-size-20 color-purple my-5 font-weight-600">Idade do titular</h3>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="opcao1">
              <label class="form-check-label" for="inlineRadio1">18 até 59 anos</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="opcao2">
              <label class="form-check-label" for="inlineRadio2">60 até 68 anos</label>
            </div>

            <h3 class="font-size-20 color-purple my-5 font-weight-600">Dados do titular</h3>
            <form>
              <div class="form-group">
                <label for="nome">* Nome do titular:</label>
                <input type="text" class="form-control" id="nome">
              </div>
              <div class="form-group">
                <label for="data">* Data de Nascimento</label>
                <input type="text" class="form-control" id="data">
              </div>

              <h3 class="font-size-20 color-purple my-5 font-weight-600">Mais Serviços</h3>

              <div class="form-check pb-2">
                <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="opcao1" aria-label="...">
                <label class="form-check-label" for="inlineRadio2">Empréstimo material de locomação <b class="color-red">+ R$150,00</b> </label>
              </div>
              <div class="form-check py-2">
                <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="opcao1" aria-label="...">
                <label class="form-check-label" for="inlineRadio2">Coroa de flores naturais <b class="color-red">+ R$300,00</b> </label>
              </div>
              <div class="form-check py-2">
                <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="opcao1" aria-label="...">
                <label class="form-check-label" for="inlineRadio2">Plano universitário <b class="color-red">+ R$29,90</b> </label>
              </div>
              <div class="form-check py-2">
                <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="opcao1" aria-label="...">
                <label class="form-check-label" for="inlineRadio2">Serviço de café<b class="color-red">+ R$99,90</b> </label>
              </div>

              <a href="#" class="d-inline-block mt-4 link-termo"><i class="fas fa-exclamation-circle"></i> Leia os termos e condições do serviço</a>

              <div class="form-check my-4">
                <input class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="opcao1" aria-label="...">
                <label class="form-check-label" for="inlineRadio2">Aceito Termos e condições do serviço
                </label>
              </div>

              <div class="row bg-lgrey mb-3 color-purple font-weight-600">
                <div class="col-md-8 py-3">
                  Valor dos dependentes:
                </div>
                <div class="col-md-4 py-3 text-md-right">
                  R$0,00
                </div>
              </div>
              <div class="row bg-lgrey mb-3 color-purple font-weight-600">
                <div class="col-md-8 py-3">
                  Valor do plano:
                </div>
                <div class="col-md-4 py-3 text-md-right">
                  R$0,00
                </div>
              </div>
              <div class="row bg-lgrey mb-3 color-purple font-weight-600">
                <div class="col-md-8 py-3">
                  Total:
                </div>
                <div class="col-md-4 py-3 text-md-right">
                  R$0,00
                </div>
              </div>

              <input type="submit" name="" value="Contrate agora" class="cta">

              <div class="my-4">
                <small>
                  <b>REF:</b> Plano Individial <br>
                  <b>Categoria:</b> Planos Funerários
                </small>
              </div>
            </form>
          </form>
        </div>
      </div>
    </div>
  </section>

  <!-- Diferenciais -->
  <section id="diferenciais" class="bg-lgrey incluso">
    <div class="container">
      <h3 class="text-uppercase font-size-25 font-weight-700 my-5 wow fadeInDown"><h2 class="section-title mb-5 wow fadeInDown">O que <b>esta incluso</b></h2></h3>
      <?php the_field('incluso'); ?>
    </div>
  </section>

  <section id="planos" class="internas">
    <div class="container">

      <h2 class="section-title mb-5 wow fadeInDown">Outros <b>planos</b></h2>
      <ul class="listagem-planos owl-carousel">
		  <?php
			  query_posts(array(
				'post_type' => 'nossos_planos',
				'showposts' => 6,
				'post__not_in' => array( $post->ID )
			  ));
			  if (have_posts()) : while (have_posts()) : the_post();
			  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
			?>
        <li>
          <div class="foto" style="background-image: url(<?php echo $image[0]; ?>)"></div>
							<div class="info bg-purple">
								<div class="titulo font-size-25 text-center font-weight-700 color-white text-uppercase mb-3">
									Plan <b class="font-size-30 d-block font-weight-700 mt-3"><?php the_title(); ?></b>
								</div>
								<div class="text-center">
									<a href="<?php the_permalink(); ?>" class="cta bg-transparent" title="Saiba mais">Saiba mais</a>
								</div>
							</div>
        </li>
        <?php endwhile; endif; wp_reset_query(); ?>
      </ul>
    </div>
  </section>


<?php get_footer(); ?>
