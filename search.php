<?php 
get_header();
$search_refer = $_GET["post_type"];
?>

<!-- Breadcrumbs -->
<section id="breadcrumbs" class="bg-lgrey text-uppercase font-size-12 font-weight-500">
	<div class="container">
		<a href="<?php bloginfo('url'); ?>" class="d-inline mr-3" title="Página Inicial">Home</a> <span class="mr-3 font-weight-700">+</span> <span>Busca</span>
	</div>
</section>


	<?php if ($search_refer == 'nossas_unidades') { ?>

<!-- Unidades -->
	<section id="unidades" class="paginas-internas">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-8">
					<h1 class="section-title mb-5 wow fadeInDown">Busca por <b><?php printf( __( '%s', 'twentyfifteen' ), get_search_query() ); ?></b></h1>
				</div>
				<div class="col-md-4">
					<div class="search mr-lg-3">
						<i class="fas fa-search font-size-12"></i>
						<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" class="d-inline">
							<input type="text" class="form-control" placeholder="Buscar Unidade" required name="s" />
							<input type="hidden" name="post_type" value="nossas_unidades" />
						</form>
					</div>
				</div>
			</div>

			<?php
			if (have_posts()) :
				?>
				<ul class="listagem-unidade row">
					<?php while (have_posts()) : the_post();
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
					?>
					<li class="col-lg-4 col-md-6 mb-5">
						<div class="bg-white text-center">
							<i class="fas fa-map-marker-alt mb-4"></i>
							<h3 class="font-size-20 font-weight-700 text-uppercase"><?php the_title(); ?></h3>
							<?php the_content(); ?>
							<p>
								<?php if(get_field('whatsup')) { ?>
									<b class="d-block mb-2">
										<i class="fab fa-whatsapp color-green"></i> <a href="https://api.whatsapp.com/send?phone=55<?php the_field('whatsup'); ?>" target="_blank" title="Entre em contato pelo WhatsApp"><?php the_field('whatsup'); ?></a>
									</b>
								<?php } ?>
								<?php if(get_field('telefone')) { ?>
									<b class="d-block"><i class="fas fa-phone-alt color-purple"></i> <?php the_field('telefone'); ?></b>
								<?php } ?>
							</p>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
			<?php the_posts_pagination(array('prev_text' => __( '<', 'textdomain' ),'next_text' => __( '>', 'textdomain' )) ); ?>
		<?php else: ?>
			<h3 class="font-size-20 cor-roxo font-weight-800 text-center">Nenhum resultado encontrado</h3>
		<?php endif; wp_reset_query(); ?>
	</div>
</section>

<?php } elseif ($search_refer == 'parceiros') { ?>
<!-- Rede de Parceiros -->
	<section id="parceiro" class="paginas-internas">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-8">
					<h1 class="section-title mb-5 wow fadeInDown">Busca por <b><?php printf( __( '%s', 'twentyfifteen' ), get_search_query() ); ?></b></h1>
				</div>
				<div class="col-md-4">
					<div class="search mr-lg-3">
						<i class="fas fa-search font-size-12"></i>
						<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" class="d-inline">
							<input type="text" class="form-control" placeholder="Buscar Parceiro" required name="s" />
							<input type="hidden" name="post_type" value="parceiros" />
						</form>
					</div>
				</div>
			</div>
			<div class="table-responsive">
				<table class="table table-striped">
					<thead class="bg-purple color-white">
						<tr>
							<th scope="col">Área</th>
							<th scope="col">Nome</th>
							<th scope="col">Especialidades</th>
							<th scope="col">Endereço</th>
							<th scope="col">Cidade</th>
							<th scope="col">Telefone</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if (have_posts()) : while (have_posts()) : the_post();
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
						?>
						<tr>
							<th><?php the_field('area'); ?></th>
							<td><?php the_title(); ?></td>
							<td><?php the_field('especialidade'); ?></td>
							<td><?php the_field('endereco'); ?></td>
							<td><?php the_field('cidade'); ?></td>
							<td><i class="fas fa-phone-alt color-purple"></i> <b><?php the_field('telefone'); ?></b></td>
						</tr>
					<?php endwhile; endif; wp_reset_query(); ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
    <?php }	else { 	?>

	<!-- Notícias -->
	<section id="noticias" class="paginas-internas bg-grey">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-8">
					<h1 class="section-title mb-5 wow fadeInDown">Busca por <b><?php printf( __( '%s', 'twentyfifteen' ), get_search_query() ); ?></b></h1>
				</div>
				
				<div class="col-md-4">
					<div class="search mr-lg-3">
						<i class="fas fa-search font-size-12"></i>
						<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" class="d-inline">
							<input type="text" class="form-control" placeholder="Buscar Notícias" required name="s" />
							<input type="hidden" name="post_type" value="post" />
						</form>
					</div>
				</div>
			</div>
			<?php
			if (have_posts()) :
				?>
				<ul class="listagem-noticias row">
					<?php
					while (have_posts()) : the_post();
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
					?>
					<li class="col-lg-4 col-md-6 mb-5">
						<a href="<?php the_permalink();?>" title="Saiba mais">
						<div class="foto" style="background-image: url(<?php echo $image[0]; ?>)"> </div>
						<div class="info bg-white">
							<div class="data color-red font-size-12">
							 <?php the_time('j/m/Y'); ?>
							</div>
							<h3 class="font-size-20 font-weight-600"><?php the_title(); ?> </h3>
						   <div class="resumo">
							 <?php the_excerpt(); ?>
						   </div>
							<span class="botao-final"><i class="bg-red">+</i>  Saiba mais</span>
						</div>
						</a>
					</li>
				<?php endwhile; ?>
			</ul>
			<?php the_posts_pagination(array('prev_text' => __( '<', 'textdomain' ),'next_text' => __( '>', 'textdomain' )) ); ?>
		<?php else: ?>
			<h3 class="font-size-20 cor-roxo font-weight-800 text-center">Nenhum resultado encontrado</h3>
		<?php endif; ?>
	</div>
</section>

<?php } ?>

<?php get_footer(); ?>
