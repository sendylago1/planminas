<?php
get_header();
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
global $post;
$children = get_pages( array( 'child_of' => $post->ID ) );
?>

<!-- Breadcrumbs -->
<section id="breadcrumbs" class="bg-lgrey text-uppercase font-size-12 font-weight-500">
	<div class="container">
		<a href="<?php echo network_site_url(); ?>" class="d-inline mr-3" title="Página Inicial">Home</a> <span class="mr-3 font-weight-700">+</span> <span><?php the_title(); ?></span>
	</div>
</section>

<?php if ( !is_woocommerce() ) { ?>
	<?php if (is_page(8)) { ?>
		<!-- Sobre -->
		<section id="sobre">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-6 txt-left">
						<h2 class="section-title mb-5 wow fadeInDown">Sobre<b>a Plan Minas</b></h2>
						<div class="wow fadeIn">
							<?php the_content(); ?>
						</div>
						<a href="<?php echo network_site_url(); ?>/planos" class="cta my-4 wow fadeInUp" title="Faça seu plano">Faça seu plano</a>
					</div>
					<div class="col-lg-6">
						<img src="<?php echo $image[0]; ?>" class="img-fluid mb-4 wow fadeInRight" alt="<?php the_title(); ?>">
					</div>
				</div>
			</div>
		</section>

		<!-- Diretrizes -->
		<section id="diretrizes">
			<div class="container">
				<div class="row align-items-center mb-5">
					<div class="offset-md-2 col-lg-2 mb-4 text-center wow fadeIn">
						<div class="icone mb-4">
							<div class="bg-red p-3">
								<img src="<?php the_field('iconede_1'); ?>" alt="Missão" class="img-fluid">
							</div>
						</div>
					</div>
					<div class="col-lg-6 mb-4 wow fadeIn">
						<h3 class="text-uppercase font-size-25 font-weight-700">Missão</h3>
						<p>
							<?php the_field('textode_1'); ?>
						</p>
					</div>
				</div>
				<div class="row align-items-center mb-5">
					<div class="offset-md-2 col-lg-2 mb-4 text-center wow fadeIn">
						<div class="icone mb-4">
							<div class="bg-red p-3">
								<img src="<?php the_field('iconede_2'); ?>" alt="Visão" class="img-fluid">
							</div>
						</div>
					</div>
					<div class="col-lg-6 mb-4 wow fadeIn">
						<h3 class="text-uppercase font-size-25 font-weight-700">Visão</h3>
						<p>
							<?php the_field('textode_2'); ?>
						</p>
					</div>
				</div>
				<div class="row align-items-center mb-5">
					<div class="offset-md-2 col-lg-2 mb-4 text-center wow fadeIn">
						<div class="icone mb-4">
							<div class="bg-red p-3">
								<img src="<?php the_field('iconede_3'); ?>" alt="Valores" class="img-fluid">
							</div>
						</div>
					</div>
					<div class="col-lg-6 mb-4 wow fadeIn">
						<h3 class="text-uppercase font-size-25 font-weight-700">Valores</h3>
						<p>
							<?php the_field('textode_3'); ?>
						</p>
					</div>
				</div>
			</div>
		</section>

		<!-- Diferenciais -->
		<section id="diferenciais" class="bg-lgrey">
			<div class="container-fluid">
				<div class="row align-items-center">
					<div class="col-md-5 pl-md-0">
						<ul class="listagem-fotos owl-carousel">
							<?php
							$images = acf_photo_gallery('galeria_fotos', $post->ID);
							if( count($images) ):
								foreach($images as $image):
									$full_image_url= $image['full_image_url'];
									$url= $image['url'];
									$target= $image['target'];
									?>
									<li>
										<?php if( !empty($url) ){ ?><a href="<?php echo $url; ?>" <?php echo ($target == 'true' )? 'target="_blank"': ''; ?>><?php } ?>
											<a href="<?php echo $full_image_url; ?>" data-fancybox="galeria" style="background-image: url(<?php echo $full_image_url; ?>);">
											</a>
											<?php if( !empty($url) ){ ?></a><?php } ?>
										</li>
									<?php endforeach; endif; ?>
								</ul>
							</div>
							<div class="col-md-7 txt-right pl-md-5">
								<h3 class="text-uppercase font-size-25 font-weight-700 my-5 wow fadeInDown">Porque somos diferentes!</h3>
								<?php the_field('texto_di'); ?>
							</div>
						</div>
					</div>
				</section>

			<?php } ?>

			<?php if (is_page(10)) { ?>

				<section id="planos" class="internas">
					<div class="container">
						<div class="row">
							<div class="col-md-4">
								<h2 class="section-title mb-5 wow fadeInDown">Faça<b>seu plano</b></h2>
							</div>
							<div class="col-md-8 d-md-flex align-items-center wow fadeInDown">
								<div class="mr-4 mb-3">
									<img src="<?php echo get_template_directory_uri(); ?>/img/icone-call.png" alt="Central de Atendimento">
								</div>
								<div>
									Todos os nossos planos são completos. Entre em contato com nossa central de atendimento e <b class="text-uppercase">contrate agora o seu plano</b>.
								</div>
							</div>
						</div>
						<?php
						query_posts('post_type=nossos_planos&posts_per_page=9&paged='.$paged);
						if (have_posts()) :
							?>
							<ul class="row listagem-planos">
								<?php while (have_posts()) : the_post();
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
								?>
								<li class="col-xl-4 col-lg-6 mb-5
								<?php
								if( get_field('destacar') != '' ) {
									echo 'destaque';
								}
								?>"
								>
								<div class="foto" style="background-image: url(<?php echo $image[0]; ?>)"></div>
								<div class="info bg-purple">
									<div class="titulo font-size-25 text-center font-weight-700 color-white text-uppercase mb-3">
										Plan <b class="font-size-30 d-block font-weight-700 mt-3"><?php the_title(); ?></b>
									</div>
									<small class="d-block color-branca text-center font-size-12"><?php the_field('adesao'); ?></small>
									<div class="preco font-size-20 color-white text-uppercase my-5 text-center">
										R$ <b class="font-size-50 font-weight-600"><?php the_field('mensal'); ?></b> / mês
									</div>
									<div class="text-center">
										<a href="<?php the_permalink(); ?>" class="cta mb-3" title="Adquirir">Adquirir</a> <br>
										<a href="<?php the_permalink(); ?>" class="cta bg-transparent" title="Saiba mais">Saiba mais</a>
									</div>
								</div>
							</li>
						<?php endwhile; ?>
					</ul>
					<?php the_posts_pagination(array('prev_text' => __( '<', 'textdomain' ),'next_text' => __( '>', 'textdomain' )) ); ?>
				<?php else: ?>
					<h3 class="font-size-20 cor-roxo font-weight-800 text-center">Nenhum resultado encontrado</h3>
				<?php endif; wp_reset_query(); ?>
			</div>
		</section>


	<?php } ?>


	<?php if (is_page(12)) { ?>

		<section id="servicos">
			<ul class="listagem-servicos">
				<?php
				query_posts('post_type=nossos_servicos&showposts=-1');
				if (have_posts()) : while (have_posts()) : the_post();
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
				?>
				<li>
					<div class="container">
						<div class="row">
							<div class="col-lg-6">
								<h2 class="font-size-40 font-weight-700 text-uppercase color-purple mb-5 wow fadeInDown"><?php the_title(); ?></b></h2>
								<div class="wow fadeIn">
									<?php the_content(); ?>
								</div>
								<a href="<?php echo network_site_url(); ?>/contato" class="cta my-4 wow fadeInUp" title="Contratar Serviço">Contratar Serviço</a>
							</div>
							<div class="col-lg-6  wow fadeIn">
								<div class="icone mb-4">
									<div class="bg-red p-3">
										<img src="<?php the_field('icone_servicos'); ?>" alt="<?php the_title(); ?>" class="img-fluid">
									</div>
								</div>
								<img src="<?php echo $image[0]; ?>" class="img-fluid mb-4" alt="<?php the_title(); ?>">
							</div>
						</div>
					</div>
				</li>
			<?php endwhile; endif; wp_reset_query(); ?>
		</ul>
	</section>

	<section id="fale-conosco" class="bg-lgrey">
		<div class="container text-center">
			<h2 class="text-uppercase color-red font-size-30 font-weight-300  mb-3">Fale Conosco</h2>
			<h3 class="text-uppercase color-purple font-size-20 font-weight-700 mb-5">estamos sempre à disposição para melhor atendê-los</h3>
			<a href="<?php echo network_site_url(); ?>/contato" class="cta" title="Fale Conosco">Fale Conosco</a>
		</div>
	</section>

	<?php } ?>


	<?php if (is_page(14)) { ?>
		<!-- Unidades -->
		<section id="unidades" class="paginas-internas">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-md-8">
						<h2 class="section-title mb-5 wow fadeInDown">Unidades <b>Encontre a unidade mais próxima de você!</b></h2>
					</div>
					<div class="col-md-4">
						<div class="search mr-lg-3">
							<i class="fas fa-search font-size-12"></i>
							<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" class="d-inline">
								<input type="text" class="form-control" placeholder="Buscar Unidade" required name="s" />
								<input type="hidden" name="post_type" value="nossas_unidades" />
							</form>
						</div>
					</div>
				</div>

				<?php
				query_posts('post_type=nossas_unidades&posts_per_page=6&paged='.$paged);
				if (have_posts()) :
					?>
					<ul class="listagem-unidade row">
						<?php while (have_posts()) : the_post();
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
						?>
						<li class="col-lg-4 col-md-6 mb-5">
							<div class="bg-white text-center">
								<i class="fas fa-map-marker-alt mb-4"></i>
								<h3 class="font-size-20 font-weight-700 text-uppercase"><?php the_title(); ?></h3>
								<?php the_content(); ?>
								<p>
									<?php if(get_field('whatsup')) { 						
									$whatsapp = get_field('whatsup');
									$reduzido = str_replace( array( ' ', '-', '(', ')' ), '', $whatsapp);
									?>
										<b class="d-block mb-2">
											<i class="fab fa-whatsapp color-green"></i> 
											<a href="https://api.whatsapp.com/send?phone=55<?php echo $reduzido; ?>" target="_blank" title="Entre em contato pelo WhatsApp">
												<?php the_field('whatsup'); ?>
											</a>
									</b>
									<?php } ?>
									<?php if(get_field('telefone')) { ?>
										<b class="d-block"><i class="fas fa-phone-alt color-purple"></i> <?php the_field('telefone'); ?></b>
									<?php } ?>
								</p>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php the_posts_pagination(array('prev_text' => __( '<', 'textdomain' ),'next_text' => __( '>', 'textdomain' )) ); ?>
			<?php else: ?>
				<h3 class="font-size-20 cor-roxo font-weight-800 text-center">Nenhum resultado encontrado</h3>
			<?php endif; wp_reset_query(); ?>
		</div>
	</section>

	<?php } ?>

	<?php if (is_page(16)) { ?>
		<!-- Rede de Parceiros -->
		<section id="parceiro" class="paginas-internas">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-md-8">
						<h2 class="section-title mb-5 wow fadeInDown">Rede de <b>Parceiros</b></h2>
					</div>
					<div class="col-md-4">
						<div class="search mr-lg-3">
							<i class="fas fa-search font-size-12"></i>
							<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" class="d-inline">
								<input type="text" class="form-control" placeholder="Buscar Parceiro" required name="s" />
								<input type="hidden" name="post_type" value="parceiros" />
							</form>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-striped">
						<thead class="bg-purple color-white">
							<tr>
								<th scope="col">Área</th>
								<th scope="col">Nome</th>
								<th scope="col">Especialidades</th>
								<th scope="col">Endereço</th>
								<th scope="col">Cidade</th>
								<th scope="col">Telefone</th>
							</tr>
						</thead>
						<tbody>
							<?php
							query_posts('post_type=parceiros&showposts=-1');
							if (have_posts()) : while (have_posts()) : the_post();
							$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
							?>
							<tr>
								<th><?php the_field('area'); ?></th>
								<td><?php the_title(); ?></td>
								<td><?php the_field('especialidade'); ?></td>
								<td><?php the_field('endereco'); ?></td>
								<td><?php the_field('cidade'); ?></td>
								<td><i class="fas fa-phone-alt color-purple"></i> <b><?php the_field('telefone'); ?></b></td>
							</tr>
						<?php endwhile; endif; wp_reset_query(); ?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
	<?php } ?>

	<?php if (is_page(20)) { ?>
		<section id="banner-contato" class="bg-purple py-0">
			<div class="container-fluid">
				<div class="row align-items-center">
					<div class="col-md-6 txt-left color-white">
						<?php the_content(); ?>

						<div class="telefone d-md-flex align-items-center my-4">
							<div class="bg-red icone mr-3">
								<i class="fas fa-phone-alt"></i>
							</div>
							<div class="font-size-14 letter-spacing-1">
								Central de Atendimento <br>
								<div class="font-size-25">
									<?php the_field('telefone_central'); ?>
								</div>
							</div>
						</div>
						
						<div class="telefone d-md-flex align-items-center my-4">
							<div class="bg-red icone mr-3">
								<img src="<?php echo get_template_directory_uri(); ?>/img/24h.png" alt="">
							</div>
							<div class="font-size-14 letter-spacing-1">
								Funerária <b class="color-red">24 horas</b> <br>
								<div class="font-size-25">
									<?php the_field('telefone_funeraria'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<img src="<?php echo $image[0]; ?>" class="img-fluid mt-5" alt="">
					</div>
				</div>
			</div>
		</section>

		<!-- Contato -->
		<section id="contato">
			<div class="container">
				<div class="row mt-5">
					<div class="col-lg-6 pr-lg-5 mb-5">
						<?php echo do_shortcode( '[contact-form-7 id="5" title="Contato"]' ); ?>
					</div>
					<div class="col-lg-6">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3702.3647208443876!2d-42.69809818480008!3d-21.882026885542366!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xa2a8122f462375%3A0x65259fea060fcb91!2sR.%20Cel.%20Osc%C3%A1r%20Cort%C3%AAs%2C%2064%20-%20107%20-%20Porto%20Novo%2C%20Al%C3%A9m%20Para%C3%ADba%20-%20MG%2C%2036660-000!5e0!3m2!1spt-BR!2sbr!4v1596230095315!5m2!1spt-BR!2sbr" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

					</div>
				</div>
			</div>
	</section>

	<?php } ?>

	<?php if (!is_page(8) && !is_page(10) && !is_page(12) && !is_page(14) && !is_page(16) && !is_page(20) ) { ?>
		<!-- Outras páginas -->
		<section id="textos-extras">
			<div class="container">
				<h2 class="font-weight-800 font-size-40 mb-5 text-center cor-laranja wow fadeInDown">aqui<?php the_title(); ?></h2>
				<div class="wow fadeInUp">
					<?php the_content(); ?>
				</div>
			</div>
		</section>
	<?php } ?>

<?php } ?>

<?php get_footer(); ?>
